﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float runningSpeed = 1.5f;
    public float jumpForce = 5f;

    private Rigidbody2D rigidbody;

    public Animator animator;
    // detect layer ground
    public LayerMask groundLayer;
// wenoweno
    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // User press Space for Jump
            Jump();
        }
    }

     void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.D))
        {
            RunningRight();
        }    

        if (Input.GetKey(KeyCode.A))
        {
            RunningLeft();

        }
    }

    void Jump()
    {
        // F = m * a ====> a = F/m
        if (isTouchingGround()) { 

        rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
    }

    void RunningRight()
    {
        if(rigidbody.velocity.x < runningSpeed)
        {
            rigidbody.velocity = new Vector2(runningSpeed, rigidbody.velocity.y);
        }

    }

    void RunningLeft()
    {
        if (rigidbody.velocity.x < runningSpeed)
        {
            rigidbody.velocity = new Vector2(-runningSpeed, rigidbody.velocity.y);
        }
    }
    bool isTouchingGround()
    {
        if (Physics2D.Raycast(this.transform.position,Vector2.down, 0.50f, groundLayer))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}




    
  
