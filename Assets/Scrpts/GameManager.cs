﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum GameState
{
    menu,
    inGame,
    gameOver
}

public class GameManager : MonoBehaviour {
    public GameState currentGameState = GameState.menu;

  public void StartGame()
    {
        SetGameState(GameState.inGame);
    }

    public void GameOver()
    {
        SetGameState(GameState.gameOver);
    }

    public void BackToTheMenu()
    {
        SetGameState(GameState.menu);
    }

    void SetGameState(GameState newGameState)
    {
        if(newGameState == GameState.menu)
        {

        } else if(newGameState == GameState.inGame)
        {

        } else if(newGameState== GameState.gameOver)
        {

        }
        this.currentGameState = newGameState;
    }

}
